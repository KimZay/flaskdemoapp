from flask import Flask, url_for, render_template, jsonify, request

import base64
import numpy as np
import io
from PIL import Image
import keras
from keras import backend as K
from keras.models import Sequential
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import img_to_array

app = Flask(__name__)

def get_mogel():
    global model
    model = load_model('models/VGG16_cats_and_dogs_2.h5')
    print(" * Model loaded!")

print(" * Loading Keras model...")
get_mogel()

@app.route('/')
def index():
    return "Go to predict 0.1.4"

@app.route('/predict')
def predict():
    return render_template("predict.html")

@app.route('/predict', methods=['POST'])
def predictML():
    message = request.get_json(force=True)

    response = {
        'prediction': {
            'answer': 'test_answer'
         }
    }

    return jsonify(response)